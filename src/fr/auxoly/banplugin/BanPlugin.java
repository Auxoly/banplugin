package fr.auxoly.banplugin;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class BanPlugin extends JavaPlugin implements Listener {

    @Override
    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(this, this);
        saveDefaultConfig();
        System.out.println("[BanPlugin] Enabled !");
    }

    @Override
    public void onDisable() {
        System.out.println("[BanPlugin] Disabled !");
    }

    @EventHandler
    public void playerbreakBlock(PlayerKickEvent event) throws IOException {
        Player p = event.getPlayer();
        if(p.isBanned()) {
            try{
                FileWriter reportBanWriter = new FileWriter(".\\plugins\\BanPlugin\\" + p.getName() + " ban report.json");
                reportBanWriter.write("{\n" + "  \"PSEUDO\": \"" + p.getName() + "\"\n  UUID\": " + p.getUniqueId() + "\"\n  EXPERIENCE: \"" + p.getLevel() + "\"\n  Vie: \"" + p.getHealth() + "\"\n}");
                reportBanWriter.close();
                Bukkit.broadcastMessage("§6[BanPlugin] §eLe joueur §6" + p.getName() + " §ea été banni !");
            }catch (IOException e) {
                System.out.println("Impossible de creer le fichier rapport !");
                e.printStackTrace();
            }

        }
    }

}
